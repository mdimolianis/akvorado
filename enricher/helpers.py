def convert_dictionary_to_list(dictionary):
    final_list = []
    for k,v in dictionary.items():
        final_list.append({"name":k, "macs":v})
    return(final_list)

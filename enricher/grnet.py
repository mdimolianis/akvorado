from jinja2 import Environment, FileSystemLoader
import requests
from markupsafe import Markup
from helpers import convert_dictionary_to_list
'''
The main idea is to retrieve data from the IXPManager and use them to create queries at Akvorado UI.
We get three types of data:
-Per switch
-Per point of presence
-Per Internet Exchange (per VLAN, this is a convention)
We want to get the MAC addresses that correspond to the aformentioned categories and also generate a naming convention for searching at Akvorado:
- member-name@VLAN@switch-name
- member-name@POP
- member-name@VLAN

We create three dictionaries and then convert them to list in order to use them as input to a jinja2 file that creates all of the required queries
'''

def enrich_grnet_based_on_members():
    asn_to_member_name={}
    asn_to_member_name_list=[]
    #retrieve data from members.grnet.gr
    r = requests.get('https://members.grnet.gr/api/aslist/', 
                    headers={'Accept': 'application/json'})
    members_api_data = r.json()
    members_data = members_api_data["grnet_members"]
    for member in members_data:
        asn = members_data[member]["as"]
        member_name = members_data[member]["slug"]
        # Removing quotes from names to feed it to akvorado"
        member_name = member_name.replace('"','')
        # If asn and member name are valid and also if the asn is private
        if asn and member_name and (int(asn)>=64512 and int(asn)<=65534):
            asn_to_member_name[asn]= member_name
    for k,v in asn_to_member_name.items():
        asn_to_member_name_list.append({"asn":k,"name":v})

    akvorado_config_file = "/srv/akvorado/config/akvorado.yaml"
    akvorado_config_file_template = "akvorado.yaml.j2"
    environment = Environment(loader=FileSystemLoader("/srv/akvorado/enricher/"))
    template = environment.get_template(akvorado_config_file_template)
    context = {"member_data": asn_to_member_name_list}
    with open(akvorado_config_file, mode="w", encoding="utf-8") as results:
        results.write(template.render(context))


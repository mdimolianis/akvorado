from jinja2 import Environment, FileSystemLoader
import requests
from markupsafe import Markup
from helpers import convert_dictionary_to_list
from config import APIKEY
'''
The main idea is to retrieve data from the IXPManager and use them to create queries at Akvorado UI.
We get three types of data:
-Per switch
-Per point of presence
-Per Internet Exchange (per VLAN, this is a convention)
We want to get the MAC addresses that correspond to the aformentioned categories and also generate a naming convention for searching at Akvorado:
- member-name@VLAN@switch-name
- member-name@POP
- member-name@VLAN

We create three dictionaries and then convert them to list in order to use them as input to a jinja2 file that creates all of the required queries
'''

def enrich_grix():
    #retrieve data from IXPManager
    r = requests.get('https://portal.gr-ix.gr/api/v4/grix/list/?apikey='+str(APIKEY), 
                    headers={'Accept': 'application/json'})
    ixpm_api_data = r.json()
    members_data = ixpm_api_data["ixpmanager_export"]["member_list"]
    
    # Dictionaries using as key what the name of the dict implies and as value the corresponding MAC addresses
    member_data_per_switch = {}
    member_data_per_point_of_presence = {}
    member_data_per_internet_exchange = {}
    
    for member in members_data:
        for connection_list in member["connection_list"]:
            # PoP derived from switch name based on the first word used in the hostname e.g. tis-sw1, get characters until finding -
            switch_name = connection_list["switch_name"][0:3]
            point_of_presence = switch_name.split("-")
            point_of_presence = point_of_presence[0]
            point_of_presence = point_of_presence.upper()
            # Member name concatenated with the PoP name used as an index for all MAC of a PoP
            member_name_and_pop = member["shortname"]+"@"+point_of_presence
            # Initialize the member in the PoP
            if member_name_and_pop not in member_data_per_point_of_presence:
                member_data_per_point_of_presence[member_name_and_pop]= []
            # Member data per switch
            for vlans in connection_list["vlan_list"]:
                # Vlan indicates the IX, this is a convention, e.g. VLAN 505 is GR-IX Athens
                members_name_and_ix = member["shortname"]+"@"+vlans["vlan_name"]
                if members_name_and_ix not in member_data_per_internet_exchange:
                    member_data_per_internet_exchange[members_name_and_ix] = []
                if vlans["l2_addresses"]!=[]:
                    member_connection_name = member["shortname"]+"@"+vlans["vlan_name"]+"@"+connection_list["switch_name"]
                    if member_connection_name not in member_data_per_switch:
                        member_data_per_switch[member_connection_name] = []
                    member_data_per_switch[member_connection_name] = member_data_per_switch[member_connection_name] + vlans["l2_addresses"]
                    member_data_per_internet_exchange[members_name_and_ix] =  member_data_per_internet_exchange[members_name_and_ix] + vlans["l2_addresses"]
                    member_data_per_point_of_presence[member_name_and_pop] = member_data_per_point_of_presence[member_name_and_pop] + vlans["l2_addresses"]
    
    member_data_per_switch_formatted = convert_dictionary_to_list(member_data_per_switch)
    member_data_per_point_of_presence_formatted = convert_dictionary_to_list(member_data_per_point_of_presence)
    member_data_per_internet_exchange_formatted = convert_dictionary_to_list(member_data_per_internet_exchange)
    
    console_config_file = "/srv/akvorado/config/console.yaml"
    console_config_file_template = "console.yaml.j2"
    environment = Environment(loader=FileSystemLoader("/srv/akvorado/enricher/"))
    template = environment.get_template(console_config_file_template)
    context = {"member_data_per_switch": member_data_per_switch_formatted, "member_data_per_point_of_presence":member_data_per_point_of_presence_formatted, "member_data_per_internet_exchange":member_data_per_internet_exchange_formatted}
    with open(console_config_file, mode="w", encoding="utf-8") as results:
        results.write(template.render(context))

